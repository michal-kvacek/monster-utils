#!/bin/bash

if [ "$3" == "" ]; then
   echo "Usage: $0 [treshold] [mail recipient] [mail subject]"
   echo ""
   echo "Příklad: $0 75 hello@example.com \"Dochází místo na disku!\""
   echo ""
   echo "Odešle notifikaci na hello@example.com pokud zaplnění disku přesáhne 75 %."
   exit 1;
fi


TRESHOLD=$1
MAIL_TO=$2
SUBJECT=$3

DISK_ALERT=`df -H | grep -vE 'sr0|tmpfs' | grep "^/dev" | tr -s " " | cut -d " " -f1,5,6 | awk 'int($2) > '$TRESHOLD' {print $1 " (pripojeno k " $3 ") zbyva " $2 }'`

if [ "$DISK_ALERT" != "" ]; then
   echo $DISK_ALERT | mail -s "$SUBJECT" $MAIL_TO
fi

